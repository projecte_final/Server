
const mysql = require('mysql');
const express = require("express");
const PORT = process.env.PORT || 3001;
const app = express();
const cors = require('cors');

var bcrypt = require('bcrypt');
const saltRounds = 10;

/*          GETS        */
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

/*          SOCKETIO    */
var usuariBatalla = [];
const http = require('http');
const server = http.createServer(app);
const socketio = require('socket.io');
const io = socketio(server, {
    cors: {
        origin: '*',
    }
});
io.on('connection', socket => {
    socket.on('conectado', (nombre) => {
        console.log(nombre + " se ha conectado");
    });
    socket.on('buscarBatalla', (nombre, idMonstre, sessionid, monstre) => {
        console.log(nombre + " esta buscando un rival con el monstruo " + idMonstre);
        usuariBatalla.push({ nombre, idMonstre, sessionid, monstre });
        monstre.Nombre = nombre;
        if (usuariBatalla.length === 2) {            
            var registro = juego({...usuariBatalla[0].monstre}, {...usuariBatalla[1].monstre});            
            io.emit('batallaEncontrada', { usuariBatalla, registro});
            usuariBatalla = [];
        }
    })
    socket.on('cancelarBusqueda', (sessionid) => {
        usuariBatalla.splice((usuariBatalla.findIndex(x => x.sessionid == sessionid)), 1);
    })
});
server.listen(3002, () => console.log("Servidor Socket Port 3002"));


/*-----------------------*/

const connection = mysql.createConnection({
    host: 'projectefinal.csjdkgcgscjj.us-east-1.rds.amazonaws.com',
    port: 4040,
    user: 'admin',
    password: 'Patata_123',
    database: 'projecte_final'
})

app.use(cors());
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
var select1 = "";
connection.connect((err) => {

    if (err) throw err
    console.log('Conexion a base de datos: Correcta')
})
connection.query('SELECT * from Usuari', (err, rows) => {
    if (err) throw err
    this.select1 = "Registrate";

})
async function login(usu, passw) {
    await connection.query('select count(*) from Usuari where usuari = "Patata" AND contrasenya = "patata";', (err, rows) => {

        return "1";
    })
}

app.post("/api/validarUsuario", (req, res) => {
    const f_usuario = req.body.usuario;
    const f_contraseña = req.body.contraseña;
    connection.query('select contrasenya from Usuari where usuari = ?', [f_usuario], (err, rows) => {
        if (rows.length == 0) {
            res.json({ mensaje: "Usuario incorrecto" });
        }
        else {
            bcrypt.compare(f_contraseña, rows[0].contrasenya, function (err, result) {
                if (result) {
                    res.json({ mensaje: "True" });
                }
                else {
                    res.json({ mensaje: "Credenciales incorrectas" });
                }
            })
        }
    })
});

app.post("/api/ComprobarUsuario", (req, res) => {
    const c_usuario = req.body.usuario;
    var msg = "";
    connection.query('select * from Usuari where usuari = ? ', [c_usuario,], (err, rows) => {
        if (rows.length == 0) {
            res.json({ mensaje: "false" });
        } else {
            res.json({ mensaje: "true" });
        }
    })
});
app.post("/api/admin_eliminarmonstre", (req, res) => {
    var idmonstre = req.body.idmonstruo;
    connection.query('delete from usuari_monstres where idMonstre = ? ', [idmonstre,], (err, rows) => {
    console.log("eliminado");
    })
    connection.query('delete from Monstre where idMonstre = ? ', [idmonstre,], (err, rows) => {
        console.log("elimiinado");
        })
});
app.post("/api/crear_monstre", (req, res) => {
    var idmonstre = 0;
    var nommonstre = req.body.nommonstre;
    var imgmonstre =req.body.imgmonstre;
    var gifmonstre = req.body.gifmonstre;
    var descripciomonstre = req.body.descripciomonstre;
    var nivellmonstre = req.body.nivellmonstre;
    var vidamonstre = req.body.vidamonstre;
    var danymonstre = req.body.danymonstre;
    var armaduramonstre = req.body.armaduramonstre;
    var esquivamonstre = req.body.esquivamonstre;
    
connection.query('select count(idMonstre) as nextid from Monstre ', (err, rows) => {
       idmonstre = rows[0].nextid+1;
        })
 connection.query('INSERT INTO Monstre ( idMonstre, img, Nom, Descripcio, img_gif, nivel, Dany, Vida,Armadura,Esquiva)VALUES (?,?,?,?,?,?,?,?,?,?); ',
  [idmonstre,imgmonstre,nommonstre,descripciomonstre,gifmonstre,nivellmonstre,danymonstre,vidamonstre,armaduramonstre,esquivamonstre], (err, rows) => {
});

});
app.post("/api/crearusuari", (req, res) => {
    var usuario = req.body.usuario;
    var contrasenya = req.body.contrasenya;
    var correu = req.body.correu

 connection.query('INSERT INTO Usuari ( usuari, contrasenya, correu)VALUES (?,?,?); ',
  [usuario,contrasenya,correu], (err, rows) => {
});
});
app.post("/api/eliminarusuario", (req, res) => {
    var usuario = req.body.usuario;
    connection.query('delete from usuari_monstres where Usuari = ? ', [usuario], (err, rows) => {
        console.log("eliminado");
        })
        connection.query('delete from Usuari where usuari = ? ', [usuario], (err, rows) => {
            console.log("elimiinado");
            })
});
app.post("/api/GetMonstruos", (req, res) => {
    const usuario = req.body.usuario;

    connection.query('select * from usuari_monstres where Usuari = ? ', [usuario,], (err, rows) => {

        if (rows.length != 0) {
            res.json({ rows });
        } else {
            res.json({ mensaje: "False" });
        }
    })
});

app.post("/api/GetImgMonstruos", (req, res) => {
    const usuario = req.body.usuario;
    connection.query('select  Monstre.Nom,Monstre.idMonstre, Monstre.img from Monstre, usuari_monstres where usuari_monstres.Usuari = ? AND Monstre.idMonstre = usuari_monstres.idMonstre;', [usuario], (err, rows) => {
        if (rows.length != 0) {
            res.json({ rows });
        } else {
            res.json({ mensaje: "False" });
        }
    })
});
app.post("/api/contadormonstres", (req, res) => {
    const usuario = req.body.usuario;
    connection.query('SELECT count(Usuari) as usuario FROM usuari_monstres where Usuari = ?;', [usuario], (err, rows) => {
        //  if(rows[0].usuario){
        //     res.json({ mensaje: "True" });
        //  }else{
        //     res.json({ mensaje: "False" });
        //  }
        if (rows[0].usuario > 1) {
            res.json({ mensaje: "True" });
        } else {
            res.json({ mensaje: "False" });
        }
    })
});
app.post("/api/GetMonstruo_info", (req, res) => {
    const usuario = req.body.usuario;
    const idmonstruo = req.body.idmonstruo;
    connection.query('select usuari_monstres.idMonstre, usuari_monstres.Dany, usuari_monstres.Vida, usuari_monstres.Armadura, usuari_monstres.Esquiva, usuari_monstres.Punts_Gastats, usuari_monstres.Punts_Actius, usuari_monstres.Nombre_Perso, usuari_monstres.wins, usuari_monstres.losses, Monstre.img, Monstre.Nom, Monstre.Descripcio, Monstre.img_gif from Monstre, usuari_monstres where usuari_monstres.Usuari = ? AND Monstre.idMonstre = usuari_monstres.idMonstre AND usuari_monstres.idMonstre = ?;', [usuario, idmonstruo], (err, rows) => {
        if (rows.length != 0) {

            res.json({ rows });
        } else {
            res.json({ mensaje: "False" });
        }
    })
});

app.post("/api/InsertarUsuario", (req, res) => {
    const r_usuario = req.body.usuario;
    const r_contraseña = req.body.contraseña;
    const r_gmail = req.body.gmail;
    var numeros = remenarNumeros(valuesGenerats());
    const monstruo = Math.trunc(Math.random() * (4 - 1) + 1);
    bcrypt.hash(r_contraseña, saltRounds, function (err, hash) {
        connection.query('INSERT INTO `projecte_final`.`Usuari` ( `usuari`, `contrasenya`, `correu`) VALUES ( ?, ?, ?);', [r_usuario, hash, r_gmail], (err, rows) => {
            res.json({ mensaje: "Registro Completado" });
        })
        connection.query('insert into usuari_monstres (Usuari, idMonstre, Dany, Vida, Armadura, Esquiva, Punts_Gastats, Punts_Actius, wins, losses) values (?, ?, ?, ?, ?, ?, 0, 0, 0, 0);', [r_usuario, monstruo, numeros[0], numeros[1], numeros[2], numeros[3]])
    })

});




app.post("/changePassword", (req, res) => {
    const usuari = req.body.usuari;
    const newPassword = req.body.newPassword;
    bcrypt.hash(newPassword, saltRounds, function (err, hash) {
        connection.query('UPDATE Usuari SET contrasenya = ? WHERE usuari= ?', [hash, usuari], (err, rows) => {
            res.json({ mensaje: "Contrasenya Modificada" });
        })
    })
})

app.post("/changeUsuari", (req, res) => {
    const usuari = req.body.usuari;
    const newUser = req.body.newUsuari;
    connection.query('UPDATE Usuari, usuari_monstres SET Usuari.usuari= ?, usuari_monstres.Usuari= ? where Usuari.usuari= ? AND usuari_monstres.Usuari= ?', [newUser, newUser, usuari, usuari], (err, rows) => {
        res.json({ mensaje: "Usuari Modificat" });
    })
    /*           */

})
app.post("/changeCorreu", (req, res) => {
    const usuari = req.body.usuari;
    const newCorreu = req.body.correu;
    connection.query('UPDATE Usuari set correu= ? where usuari= ?', [newCorreu, usuari], (err, rows) => {
        res.json({ mensaje: "Correu Modificat" });
    })
});
app.post("/api/cambiar_stats", (req, res) => {
    const r_usuario = req.body.usuario;
    const id_monstre = req.body.idMonstre;
    const nouDany = req.body.Dany;
    const nuevavida = req.body.Vida;
    const nuevaarmadura = req.body.Armadura;
    const nuevaesquiva = req.body.Esquiva;
    const Punts_Actius = req.body.PuntosActivos;
    const Punts_Gastats = req.body.PuntosGastados;

    connection.query('UPDATE usuari_monstres SET Punts_Actius = ?,Punts_Gastats = ?, Dany = ? ,Vida = ?, Armadura = ?, Esquiva = ? WHERE  Usuari = ? and idMonstre = ?;', [Punts_Actius, Punts_Gastats, nouDany, nuevavida, nuevaarmadura, nuevaesquiva, r_usuario, id_monstre], (err, rows) => {
        res.json({ mensaje: "daño subido" });

    })
    /*           */

});
app.post("/api/restar_actius", (req, res) => {
    const r_usuario = req.body.usuario;
    const id_monstre = req.body.idMonstre;
    const punts_Ac = req.body.actius - 1;
    connection.query('UPDATE usuari_monstres SET Punts_Actius = ? WHERE  Usuari = ? and idMonstre = ?;', [punts_Ac, r_usuario, id_monstre], (err, rows) => {
        res.json({ mensaje: "daño subido" });
        console.log("complete");
    })
    /*           */

});
app.post("/api/cambiar_nombre", (req, res) => {
    const usuario = req.body.usuario;
    const monsutro = req.body.Monstre;
    const nuevonombre = req.body.nuevonombre;
    connection.query('UPDATE usuari_monstres SET Nombre_Perso = ? WHERE  Usuari = ? and idMonstre = ?;', [nuevonombre, usuario, monsutro], (err, rows) => {
        res.json({ mensaje: "daño subido" });
        console.log("complete");
    })
})
app.post("/api/nuevmonstruo",(req,res)=>{
    const usuario = req.body.usuario;
    const monsutro = req.body.id;
    connection.query('select count(*) idMonstres from usuari_monstres Where Usuari = ? and idMonstre = ?;',[usuario,monsutro], (err, rows) => {
        if(rows[0].idMonstres == 0){
           
            connection.query('INSERT INTO usuari_monstres ( idMonstre, Usuari, Dany, Vida, Armadura, Esquiva, Punts_Gastats, Punts_Actius,wins,losses) VALUES ( ?, ?, 1, 1,1,1,4,1,0,0);',[monsutro,usuario], (err, rows)=> {  console.log("no tiene el monstruo");});
        }else{
           res.json({mensaje: "false"})
        }
      
    })
})

app.post("/api/eliminarmonstre", (req, res) => {
    const usuario = req.body.usuario;
    const mons = req.body.monstruo;
    console.log("elimnar")
    connection.query('DELETE FROM usuari_monstres WHERE idMonstre  = ? and Usuari = ?', [mons, usuario], (err, rows) => {
        console.log("elimnar")
        
    })
})

app.get("/api/enviar", (req, res) => {
    res.json({ mensaje: this.select1 });
});
/*----------------INFO_USUARIO_PERFIL--------------*/
app.get("/api/usuario_logged", (req, res) => {
    connection.query('SELECT * from Usuari', (err, rows) => {
        res.json({ mensaje: rows });
    })

});
/*--------------------Monstruo_Usuario---------------*/
app.get("/api/Monstruos_Usuarios", (req, res) => {


});
app.post("/api/sumarvictoria", (req, res) => {
    const usuari = req.body.usuario;
    const idmonstruo = req.body.idmonstruo;
    var winsactuales = 0;

    connection.query('SELECT wins FROM usuari_monstres WHERE usuari= ? and idMonstre = ?', [usuari, idmonstruo], (err, rows) => {
        connection.query('UPDATE usuari_monstres set wins = ?  where Usuari= ? and idMonstre = ?', [rows[0].wins + 1, usuari, idmonstruo], (err, rows) => {
        })
    })

})
app.post("/api/sumarpuntos", (req, res) => {
    const usuari = req.body.usuario;
    const idmonstruo = req.body.idmonstruo;

    connection.query('SELECT Punts_Actius FROM usuari_monstres WHERE usuari= ? and idMonstre = ?', [usuari, idmonstruo], (err, rows) => {
        connection.query('UPDATE usuari_monstres set Punts_Actius = ?  where Usuari= ? and idMonstre = ?', [rows[0].Punts_Actius + 1, usuari, idmonstruo], (err, rows) => {
            console.log("punts_Actius");
        })
    })

})
app.post("/api/sumarderrota", (req, res) => {
    const usuari = req.body.usuario;
    const idmonstruo = req.body.idmonstruo;
    var winsactuales = 0;

    connection.query('SELECT losses FROM usuari_monstres WHERE usuari= ? and idMonstre = ?', [usuari, idmonstruo], (err, rows) => {
        connection.query('UPDATE usuari_monstres set losses = ?  where Usuari= ? and idMonstre = ?', [rows[0].losses + 1, usuari, idmonstruo], (err, rows) => {
        })
    })

})
app.get("/api/tiposMonstruos", (req, res) => {

    connection.query('Select * From Monstre;', (err, rows) => {
        if (rows.length != 0) {
            console.log(rows.length);
            res.json({ x: rows });
        } else {
            res.json({ mensaje: "False" });
        }
    })
        
})

/*-------------------CojerLiga--------------------------------*/

/*-------------------------------------------------*/
app.get("/api", (req, res) => {
    res.json({ message: "hola" });
});

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});

app.get("/api/GetSettingsUsuari/:usuari", urlencodedParser, (req, res) => {
    const usuari = req.params.usuari;
    connection.query('select * from vwSettingsUsuari where usuari = ?;', [usuari], (err, rows) => {
        res.json(rows);
    })
});

// app.get("/api/GetCorreuUsuari/:usuari", urlencodedParser, (req, res) =>{
//     const usuari = req.params.usuari;
//     connection.query('select correu from Usuari where usuari=?', [usuari], (err, rows) =>{
//         res.json(rows);
//     });
// })

function valuesGenerats() {
    var numeros = [];
    var max = 8;
    var ac = 0;
    for (var i = 3; i > 0; i--) {
        var numero = Math.trunc(Math.random() * (max - 1) + 1);
        ac += numero;
        max = 10 - ac - (i - 1);
        numeros.push(numero);
    }
    numeros.push(10 - ac);
    return (numeros);
}

function remenarNumeros(numeros) {
    var indiceaEliminar = Math.trunc(Math.random() * 4);
    var primer = numeros[indiceaEliminar];
    numeros.splice(indiceaEliminar, 1);

    indiceaEliminar = Math.trunc(Math.random() * 3);
    var segon = numeros[indiceaEliminar]
    numeros.splice(indiceaEliminar, 1);

    indiceaEliminar = Math.trunc(Math.random() * 2);
    var tercer = numeros[indiceaEliminar]
    numeros.splice(indiceaEliminar, 1);
    var quart = numeros[0];
    return [primer, segon, tercer, quart]
}
app.get("/api/numeroMonstres", (req, res) => {
    connection.query('select count(*) as monstres from Monstre ;', (err, rows) => {
        res.json(rows);
    })
});

app.get("/api/getImatgeMonstre/:monstreId", urlencodedParser, (req, res) => {
    const monstreId = req.params.monstreId;
    connection.query(' select img_gif from Monstre where idMonstre = ?;', [monstreId], (err, rows) => {
        res.json(rows);
    })  
})

app.get("/api/numeroUsuaris", (req, res) => {
    connection.query(' select count(*) as usuari from Usuari ', (err, rows) => {
        res.json(rows);
    })  
})
app.get("/api/getUsuaris", (req, res) => {
    connection.query(' select usuari from Usuari ', (err, rows) => {
        res.json(rows);
    })  
})
app.post("/api/enviarSolicitud", (req, res) => {
    const usuari1 = req.body.usuari1;
    const usuari2 = req.body.usuari2;
    connection.query('INSERT INTO Amics ( usuari1, usuari2, stat) VALUES ( ?, ?, false); ;', [usuari1, usuari2], (err, rows) => {
        res.json({ mensaje: "Solicitud Enviada" });
    }) 
});
app.post("/api/mostrarSolicituds", (req, res) => {
    const usuari2 = req.body.usuari2;
    connection.query('select * from Amics where usuari2 = ? && stat = false' , [usuari2], (err, rows) => {
        res.json(rows);
    })
});

app.post("/api/acceptarSolicitud", (req, res) => {
    const usuari1 = req.body.usuari1;
    const usuari2 = req.body.usuari2;
    connection.query('update Amics SET stat = true where usuari1 = ? AND usuari2 = ?;', [usuari1, usuari2], (err, rows) => {
        res.json({ mensaje: "Solicitud Acceptada" });
    }) 
});
app.post("/api/llistatAmics", (req, res) => {
    const usuari2 = req.body.usuari2;
    connection.query('select * from Amics where (usuari2 = ? OR usuari1 = ?) AND stat = true' , [usuari2, usuari2], (err, rows) => {
        res.json(rows);
    })
});
function juego(player1, player2) {
    var triaTorn = Math.trunc(Math.random() * (2 - 0) + 0);
    var torn;
    var defensaMajorP1 = false;
    var defensaMajorP2 = false;
    var vuelta = 1;
    var danyPlayer1;
    var danyPlayer2;
    var registroPartida = [];
    var arrayTemporal = [];

    if (triaTorn == 0) {
        torn = true;
    }
    else {
        torn = false;
    }
    if (player1.Esquiva < player1.Armadura) {
        defensaMajorP1 = true;
    }
    if (player2.Esquiva < player2.Armadura) {
        defensaMajorP2 = true;
    }
    while (player1.Vida >= 1 && player2.Vida >= 1) {
        if (vuelta == 1) {
            if (torn) {
                var respuesta = turno(defensaMajorP1, player1.Dany, player1.Armadura, player1.Esquiva, player1.Vida);
                torn = false;
                danyPlayer1 = respuesta[0];
                arrayTemporal.push({Nombre: player1.Nombre, Vida: player1.Vida, Dany:respuesta[0]});
            }
            else {
                var respuesta = turno(defensaMajorP2, player2.Dany, player2.Armadura, player2.Esquiva, player2.Vida);
                torn = true;
                danyPlayer2 = respuesta[0];
                arrayTemporal.push({Nombre: player2.Nombre, Vida: player2.Vida, Dany:respuesta[0]});
            }
        }
        else if (torn) {
            var respuesta = turno(defensaMajorP1, player1.Dany, player1.Armadura, player1.Esquiva, player1.Vida, danyPlayer2);
            torn = false;
            danyPlayer1 = respuesta[0];
            player1.Vida = respuesta[1];
            if((vuelta % 2) == 0){
                arrayTemporal.push({Nombre: player1.Nombre, Vida: player1.Vida, Armadura: respuesta[2], Esquiva: respuesta[3], Dany:respuesta[0]});
                registroPartida.push(arrayTemporal);
                arrayTemporal = [];
            }
            else{
                arrayTemporal.push({Nombre: player1.Nombre, Vida: player1.Vida, Armadura: respuesta[2], Esquiva: respuesta[3], Dany:respuesta[0]});
            }
        }
        else {
            var respuesta = turno(defensaMajorP2, player2.Dany, player2.Armadura, player2.Esquiva, player2.Vida, danyPlayer1);
            torn = true;
            danyPlayer2 = respuesta[0];
            player2.Vida = respuesta[1];
            if((vuelta % 2) == 0){
                arrayTemporal.push({Nombre: player2.Nombre, Vida: player2.Vida, Armadura: respuesta[2], Esquiva: respuesta[3], Dany:respuesta[0]});
                registroPartida.push(arrayTemporal);
                arrayTemporal = [];
            }
            else{
                arrayTemporal.push({Nombre: player2.Nombre, Vida: player2.Vida, Armadura: respuesta[2], Esquiva: respuesta[3], Dany:respuesta[0]});
            }
        }
        vuelta++;
    }
    if((vuelta % 2) == 0){
        registroPartida.push(arrayTemporal);
    }
    return registroPartida;
}

function turno(defensaMajor, atackBase, defensaBase, esquivaBase, vida, atackEnemic = 0) {
    var dado100 = 20;
    const min = 1;
    var randAtk = (min + Math.random() * (dado100 - min));
    var esquiva = -1;
    var defensa = -1;
    var atack;

    //*********CALCULO ATAQUE************/
    if (randAtk >= 15) {
        atack = atackBase * 2;
        atack = Math.round(atack + randAtk);
    } else if (randAtk <= 3) {
        atack = 0;
    } else {
        atack = Math.round(atackBase + randAtk);
    }


    var randomDeu = Math.random() * (10);

    if (randomDeu > 3) {
        if (defensaMajor) {
            defensa = calculDefensa(defensaBase);
        } else {
            esquiva = calculEsquiva(esquivaBase);
        }

    } else {
        if (!defensaMajor) {
            defensa = calculDefensa(defensaBase);
        } else {
            esquiva = calculEsquiva(esquivaBase);
        }
    }


    if (defensa > -1) {
        if (atackEnemic > defensa) {
            vida = vida - (atackEnemic - defensa);
        }
    } else {
        if (atackEnemic > esquiva) {
            vida = vida - atackEnemic;
        }
    }

    return ([atack, vida, defensa, esquiva])

}

function calculDefensa(defensaBase) {
    const min = 1;
    var defensa;
    var dado100 = 20;
    var randDef = (min + Math.random() * (dado100 - min));
    //*********CALCULO DEFENSA************/
    if (randDef >= 15) {
        defensa = defensaBase * 2;
        defensa = Math.round(defensa + randDef);
    } else if (randDef <= 3) {
        defensa = 0;
    } else {
        defensa = Math.round(defensaBase + randDef);
    }
    return (defensa);
}

function calculEsquiva(esquivaBase) {
    const min = 1;
    var dado120 = 30;
    var dado4 = 4;
    var esquiva = (min + Math.random() * (dado4 - min));

    //*********CALCULO ESQUIVA JUGADOR ************/

    if (esquiva === dado4) {
        esquiva = 0;
    } else {
        esquiva = (min + Math.random() * (dado120 - min));
        if (esquiva >= 25) {
            esquiva = 99999999;
        } else if (esquiva <= 5) {
            esquiva = 0;
        } else {
            esquiva = Math.round(esquiva + esquivaBase);
        }
    }
    return (esquiva);
}
